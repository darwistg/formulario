<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <title></title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    
                    <h1>Llene el formulario</h1>
            <div class="panel panel-info">
                
                <div class="panel-heading"><strong>Sistema de Registro</strong></div>
                <div class="panel-body">
                    
                    <form class="form-horizontal" name="prueba" action="procesar.php" method="POST">
                        
                <div class="form-group">
                    <label class="col-md-2 control-label" for="cedula">Cedula:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="cedula" placeholder="ingrese la cedula">
                    </div>   
                </div>
                        
                <div class="form-group">
                    <label class="col-md-2 control-label" for="nombre">Nombre:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="nombre" placeholder="ingrese el nombre">
                    </div>
                </div>
                        
                <div class="form-group">
                    <label class="col-md-2 control-label"for="apellido">Apellido:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="apellido" placeholder="ingrese el apellido">
                    </div>
                </div>
                        
                <div class="form-group">
                    <label class="col-md-2 control-label"for="fecha">Fecha:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="fecha" placeholder="ingrese la fecha">
                    </div>
                </div>
                        
                <div class="form-group">
                    <label class="col-md-2 control-label"for="lugarNacimiento">Lugar de Nacimiento:</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="lugarNacimiento" placeholder="ingrese lugar de nacimiento">
                    </div>
                </div>
                        
                <div class="form-group">   
                    <div class="col-md-4 col-md-offset-4">
                    <button type="submit" class="btn btn-success btn-md btn-block">Enviar</button>
                    </div>
                </div>
            </form>
            
            
      
      
      
  </div>
</div>
            </div>
            </div>
        </div>
        
        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    </body>
</html>
